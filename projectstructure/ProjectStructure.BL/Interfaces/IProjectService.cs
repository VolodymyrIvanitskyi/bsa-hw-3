﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;

namespace ProjectStructure.BL.Interfaces
{
    public interface IProjectService
    {
        IEnumerable<ProjectDTO> GetAllProjects();
        ProjectDTO GetProjectById(int id);
        void CreateProject(ProjectDTO project);
        void UpdateProject(ProjectDTO project);
        void DeleteProject(int projectId);
    }
}
