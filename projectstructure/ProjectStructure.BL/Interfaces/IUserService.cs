﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;

namespace ProjectStructure.BL.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAllUsers();
        UserDTO GetUserById(int id);
        void CreateUser(UserDTO user);
        void UpdateUser(UserDTO user);
        void DeleteUser(int userId);
    }
}
