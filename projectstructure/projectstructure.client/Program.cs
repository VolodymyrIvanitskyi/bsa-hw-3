﻿using ProjectStructure.Client.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.Client
{
    class Program
    {
        private static QueryService queryService = new QueryService();
        static void Main(string[] args)
        {
            while (true)
            {
                BasicMenu();

                var input = Console.ReadKey();
                Console.WriteLine();
                int id = -1;
                try
                {
                    switch (input.KeyChar)
                    {
                        case '1':
                            id = InputId();
                            if (id == -1)
                            {
                                IncorrectInput();
                                return;
                            }
                            GetCountTasksByUser(id);
                            break;
                        case '2':
                            id = InputId();
                            if (id == -1)
                            {
                                IncorrectInput();
                                return;
                            }
                            GetTasksForUser(id);
                            break;
                        case '3':
                            id = InputId();
                            if (id == -1)
                            {
                                IncorrectInput();
                                return;
                            }
                            GetFinishedTasksForUser(id);
                            break;
                        case '4':
                            GetTeamsWhenMembersOlderThan10Years();
                            break;
                        case '5':
                            GetUsersAlphabetically();
                            break;
                        case '6':
                            id = InputId();
                            if (id == -1)
                            {
                                IncorrectInput();
                                return;
                            }
                            GetDatafromUser(id);
                            break;
                        case '7':
                            GetDataFromProject();
                            break;
                        default:
                            IncorrectInput();
                            return;
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception.Message);
                }
            }
        }

        private static void BasicMenu()
        {
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");
            Console.WriteLine("1 - Get the number of tasks in a specific user's project (by id)");
            Console.WriteLine("2 - Get a list of user`s tasks (by id), where the name is < 45 characters");
            Console.WriteLine("3 - Get a list (id, name) from the collection of tasks completed in the current (2021) year for a specific user");
            Console.WriteLine("4 - Get a list (id, team name and user list) from the collection of teams over 10 years old, sorted by date of user registration in descending order, and grouped by team.");
            Console.WriteLine("5 - Get a list of users in alphabetical order first_name (ascending) with sorted tasks by length name (descending).");
            Console.WriteLine("6 - Get information about the last user project,\n" +
                " the total number of tasks under the last project,\n" +
                " the total number of incomplete or canceled tasks for the user,\n" +
                " The longest user task by date (earliest created - latest completed)");
            Console.WriteLine("7-Get:\n" +
                "The longest task of the project (according to the description)\n" +
                "The shortest task of the project (named)\n" +
                "Total number of users in the project team, where either the project description> 20 characters or the number of tasks < 3");
            Console.WriteLine("-----------------------------------------------------------------------------------------------------");

            Console.Write("Enter a number (1-7): ");
        }

        private static int InputId()
        {
            Console.Write("Enter Id:");
            try
            {
                return int.Parse(Console.ReadLine());
            }
            catch { return -1; }
        }
        private static void IncorrectInput()
        {
            Console.WriteLine("Incorrect input parameters!!!");
        }
        private static void GetCountTasksByUser(int id)
        {
            Dictionary<int, int> result = queryService.GetCountTasksByUser(id);

            if (result.Count > 0)
            {
                foreach (var item in result)
                {
                    Console.WriteLine("Project id: " + item.Key + ", count of tasks:" + item.Value + "\n");
                };
            }
            else
            {
                Console.WriteLine($"User with Id: {id} do not have any tasks\n");
            }
        }

        private static void GetTasksForUser(int id)
        {
            var tasks = queryService.GetTasksForUser(id);
            if (tasks.Count > 0)
            {
                foreach (var task in tasks)
                {
                    Console.WriteLine($"Task: {task.Name}, performer Id : {task.PerformerId}\n");
                }
            }
            else
            {
                Console.WriteLine($"User with id:{id} do not have any tasks");
            }
        }

        private static void GetFinishedTasksForUser(int id)
        {
            var finishedTasks = queryService.GetFinishedTasksForUser(id);
            if (finishedTasks.Count > 0)
            {
                foreach (var task in finishedTasks)
                {
                    Console.WriteLine($"Task id: {task.Id}, task name: {task.Name}");
                }
            }
            else
            {
                Console.WriteLine("User do not finish any task");
            }
        }

        private static void GetTeamsWhenMembersOlderThan10Years()
        {
            var teamWhenUsersOlderTenYear = queryService.GetTeamsWhenMembersOlderThan10Years();
            if (teamWhenUsersOlderTenYear.Count > 0)
            {
                foreach (var team in teamWhenUsersOlderTenYear)
                {
                    Console.WriteLine($"Team: {team.Name}, users:");

                    for (int i = 0; i < team.Users?.Count; i++)
                    {
                        Console.WriteLine($"First Name: {team.Users[i].FirstName}, Last Name: {team.Users[i].LastName}, Email: {team.Users[i].Email}");
                    }
                }
            }
            else
            {
                Console.WriteLine("The list is empty");
            }
        }

        private static void GetUsersAlphabetically()
        {
            var usersAlphabetically = queryService.GetUsersAlphabetically();
            foreach (var item in usersAlphabetically)
            {
                Console.WriteLine($"User: {item.FirstName}");
                if (item.Tasks?.Count > 0)
                {
                    foreach (var task in item.Tasks)
                        Console.WriteLine("Task Id: " + task.Id + "; Task name: " + task.Name);
                }
                else
                {
                    Console.WriteLine("There are no tasks");
                }
            }
        }

        private static void GetDatafromUser(int id)
        {
            var data = queryService.GetDatafromUser(id);
            if (data != null)
            {
                Console.WriteLine($"User {data.User.FirstName},\n" +
                    $"Last project: {data.LastProject?.Id}\n" +
                    $"Count of last project`s tasks: {data.CountOfTasks}\n" +
                    $"Count of canceled or unfinished tasks: {data.CountOfCanceledTasks}\n" +
                    $"The largest task: {data.TheLongestTask?.Id}");
            }
            else
            {
                Console.WriteLine("Empty data");
            }
        }

        private static void GetDataFromProject()
        {
            var dataFromProject = queryService.GetDataFromProject();
            foreach (var item in dataFromProject)
            {
                Console.WriteLine($"Project Id: {item.Project.Id}\n" +
                    $"The largest task (description): Id {item.TheShortestTask?.Id}, Name {item.TheLongestTask?.Name}\n" +
                    $"The shortest task (name): Id {item.TheShortestTask?.Id}, Name {item.TheShortestTask?.Name}\n" +
                    $"Amount count of users where description > 20 characters, or count of tasks < 3 : {item.CountOfUsers}");
            };
        }
    }
}
