﻿using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using ProjectStructure.BL.Exceptions;

namespace ProjectStructure.BL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Team> _teamRepository;

        public TeamService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _teamRepository = _unitOfWork.TeamRepository;
        }
        public void CreateTeam(TeamDTO teamDTO)
        {
            if (_teamRepository.Get(teamDTO.Id) is not null)
                throw new Exception($"Team with id {teamDTO.Id} already exists");

            var teamEntity = _mapper.Map<Team>(teamDTO);
            _teamRepository.Create(teamEntity);
            _unitOfWork.SaveChanges();
        }

        public void DeleteTeam(int teamId)
        {
            if (_teamRepository.Get(teamId) is null)
                throw new NotFoundException("Team", teamId);

            _teamRepository.Delete(teamId);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<TeamDTO> GetAllTeams()
        {
            var allTeams = _teamRepository.GetAll();
            return _mapper.Map<IEnumerable<TeamDTO>>(allTeams);
        }

        public TeamDTO GetTeamById(int teamId)
        {
            var teamEntity = _teamRepository.Get(teamId);
            if(teamEntity is null)
                throw new NotFoundException("Team", teamId);

            return _mapper.Map<TeamDTO>(teamEntity);
        }

        public void UpdateTeam(TeamDTO teamDTO)
        {
            if (_teamRepository.Get(teamDTO.Id) is null)
                throw new NotFoundException("Team", teamDTO.Id);

            var teamEntity = _mapper.Map<Team>(teamDTO);
            _teamRepository.Update(teamEntity);
            _unitOfWork.SaveChanges();
        }
    }
}
