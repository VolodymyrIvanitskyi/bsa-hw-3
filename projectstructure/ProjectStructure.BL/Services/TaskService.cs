﻿using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System.Collections.Generic;
using AutoMapper;
using ProjectStructure.DAL.Models;
using System;
using ProjectStructure.BL.Exceptions;

namespace ProjectStructure.BL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Task> _taskRepository;

        public TaskService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _taskRepository = _unitOfWork.TaskRepository;
        }
        public void CreateTask(TaskDTO taskDTO)
        {
            if (_taskRepository.Get(taskDTO.Id) is not null)
                throw new Exception($"Task with id {taskDTO.Id} already exists");

            var taskEntity = _mapper.Map<Task>(taskDTO);
            _taskRepository.Create(taskEntity);
            _unitOfWork.SaveChanges();
        }

        public void DeleteTask(int taskId)
        {
            if (_taskRepository.Get(taskId) is null)
                throw new NotFoundException("Task", taskId);

            _taskRepository.Delete(taskId);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<TaskDTO> GetAllTasks()
        {
            var alltasks = _taskRepository.GetAll();
            return _mapper.Map<IEnumerable<TaskDTO>>(alltasks);
        }

        public TaskDTO GetTaskById(int taskId)
        {
            var taskEntity = _taskRepository.Get(taskId);
            if(taskEntity is null)
                throw new NotFoundException("Task", taskId);

            return _mapper.Map<TaskDTO>(taskEntity);
        }

        public void UpdateTask(TaskDTO taskDTO)
        {
            if (_taskRepository.Get(taskDTO.Id) is null)
                throw new NotFoundException("Task", taskDTO.Id);

            var taskEntity = _mapper.Map<Task>(taskDTO);
            _taskRepository.Update(taskEntity);
            _unitOfWork.SaveChanges();
        }
    }
}
