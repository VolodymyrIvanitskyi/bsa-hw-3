﻿using ProjectStructure.Common.DTO;
using System.Collections.Generic;

namespace ProjectStructure.BL.Interfaces
{
    public interface ITeamService
    {
        IEnumerable<TeamDTO> GetAllTeams();
        TeamDTO GetTeamById(int id);
        void CreateTeam(TeamDTO team);
        void UpdateTeam(TeamDTO team);
        void DeleteTeam(int teamId);
    }
}
