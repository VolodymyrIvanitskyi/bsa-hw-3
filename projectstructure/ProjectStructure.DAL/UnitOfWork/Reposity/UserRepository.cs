﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class UserRepository : IRepository<User>, IDisposable
	{
		private readonly ProjectDBContext _dBContext;

		public UserRepository(ProjectDBContext dBContext)
		{
			_dBContext = dBContext;
		}
		public void Create(User user)
		{
			_dBContext.Users.Add(user);
		}

		public void Delete(int id)
		{

			var user = _dBContext.Users.Find(id);
			_dBContext.Users.Remove(user);
		}

		public User Get(int id)
		{
			return _dBContext.Users.Find(id);
		}

		public IEnumerable<User> GetAll()
		{
			return _dBContext.Users.ToList();
		}

		public void Update(User user)
		{
			_dBContext.Entry(user).State = EntityState.Modified;
		}

		public void Save()
		{
			_dBContext.SaveChanges();
		}

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_dBContext.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
