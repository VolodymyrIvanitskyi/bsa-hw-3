﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class TeamRepository : IRepository<Team>, IDisposable
	{
		private readonly ProjectDBContext _dBContext;

		public TeamRepository(ProjectDBContext dBContext)
		{
			_dBContext = dBContext;
		}
		public void Create(Team team)
		{
			_dBContext.Teams.Add(team);
		}

		public void Delete(int id)
		{

			var team = _dBContext.Teams.Find(id);
			_dBContext.Teams.Remove(team);
		}

		public Team Get(int id)
		{
			return _dBContext.Teams.Find(id);
		}

		public IEnumerable<Team> GetAll()
		{
			return _dBContext.Teams.ToList();
		}

		public void Update(Team team)
		{
			_dBContext.Entry(team).State = EntityState.Modified;
		}

		public void Save()
		{
			_dBContext.SaveChanges();
		}

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_dBContext.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
