﻿using AutoMapper;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System.Collections.Generic;
using System;
using ProjectStructure.BL.Exceptions;

namespace ProjectStructure.BL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Project> _projectRepository;

        public ProjectService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _projectRepository = _unitOfWork.ProjectRepository;
        }
        public void CreateProject(ProjectDTO projectDTO)
        {
            if(_projectRepository.Get(projectDTO.Id) is not null)
                throw new Exception($"Project with id {projectDTO.Id} already exists");

            var projectEntity = _mapper.Map<Project>(projectDTO);
            _projectRepository.Create(projectEntity);
            _unitOfWork.SaveChanges();
        }

        public void DeleteProject(int projectId)
        {
            if(_projectRepository.Get(projectId) is null)
                throw new NotFoundException("Project", projectId);
            _projectRepository.Delete(projectId);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            var allProjects = _projectRepository.GetAll();
            return _mapper.Map<IEnumerable<ProjectDTO>>(allProjects);
        }

        public ProjectDTO GetProjectById(int projectId)
        {
            var projectEntity = _projectRepository.Get(projectId);

            if (projectEntity is null)
                throw new NotFoundException("Project", projectId);

            return _mapper.Map<ProjectDTO>(projectEntity);
        }

        public void UpdateProject(ProjectDTO projectDTO)
        {
            if (_projectRepository.Get(projectDTO.Id) is null)
                throw new NotFoundException("Project", projectDTO.Id);

            var projectEntity = _mapper.Map<Project>(projectDTO);
            _projectRepository.Update(projectEntity);
            _unitOfWork.SaveChanges();
        }
    }
}

