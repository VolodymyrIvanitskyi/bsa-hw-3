﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class ProjectRepository : IRepository<Project>, IDisposable
	{
		private readonly ProjectDBContext _dBContext;

		public ProjectRepository(ProjectDBContext dBContext)
        {
			_dBContext = dBContext;
        }
		public void Create(Project project)
		{
			 _dBContext.Projects.Add(project);
		}

		public void Delete(int id)
		{

			var project = _dBContext.Projects.Find(id);
			_dBContext.Projects.Remove(project);
		}

		public Project Get(int id)
		{
			var result = _dBContext.Projects.Find(id);
			return result;
		}

		public IEnumerable<Project> GetAll()
		{
			return _dBContext.Projects.ToList();
		}

		public void Update(Project project)
		{
			_dBContext.Entry(project).State = EntityState.Modified;
		}

		public void Save()
        {
			_dBContext.SaveChanges();
        }

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_dBContext.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
