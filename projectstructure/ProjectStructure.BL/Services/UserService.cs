﻿using AutoMapper;
using ProjectStructure.BL.Exceptions;
using ProjectStructure.BL.Interfaces;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;

namespace ProjectStructure.BL.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<User> _userRepository;

        public UserService(IMapper mapper, IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _userRepository = _unitOfWork.UserRepository;
        }

        public void CreateUser(UserDTO userDTO)
        {
            if (_userRepository.Get(userDTO.Id) is not null)
                throw new Exception($"User with id {userDTO.Id} already exists");

            var userEntity = _mapper.Map<User>(userDTO);
            _userRepository.Create(userEntity);
            _unitOfWork.SaveChanges();
        }

        public void DeleteUser(int userId)
        {
            if (_userRepository.Get(userId) is null)
                throw new NotFoundException("User", userId);

            _userRepository.Delete(userId);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            var allUsers = _userRepository.GetAll();
            return _mapper.Map<IEnumerable<UserDTO>>(allUsers);
        }

        public UserDTO GetUserById(int userId)
        {
            var userEntity = _userRepository.Get(userId);
            if (userEntity is null)
                throw new NotFoundException("User", userId);

            return _mapper.Map<UserDTO>(userEntity);
        }

        public void UpdateUser(UserDTO userDTO)
        {
            if (_userRepository.Get(userDTO.Id) is null)
                throw new NotFoundException("User", userDTO.Id);

            var userEntity = _mapper.Map<User>(userDTO);
            _userRepository.Update(userEntity);
            _unitOfWork.SaveChanges();
        }
    }
}
