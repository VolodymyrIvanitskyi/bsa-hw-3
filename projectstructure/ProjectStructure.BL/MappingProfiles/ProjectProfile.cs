﻿
using AutoMapper;
using ProjectStructure.Common.DTO;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>();
            CreateMap<Project, ProjectDTO>();
        }
    }
}
