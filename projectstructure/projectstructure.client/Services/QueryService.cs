﻿using Newtonsoft.Json;
using ProjectStructure.Common.DTO;
using ProjectStructure.Common.DTO.QueryDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Client.Services
{
    public class QueryService
    {
        private static readonly HttpService _httpService;
        private const string queryController = "Query/";

        static QueryService()
        {
            _httpService = new HttpService();
        }

        private List<T> GetEntities<T>(string path)
        {
            string responceBody =  _httpService.GetEntities(queryController + path).Result;

            var result = JsonConvert.DeserializeObject<List<T>>(responceBody);
            return result;
        }

        private T GetEntity<T>(string path)
        {
            string responceBody = _httpService.GetEntities(queryController + path).Result;

            var result = JsonConvert.DeserializeObject<T>(responceBody);
            return result;
        }

        public Dictionary<int, int> GetCountTasksByUser(int authorId)
        {
            var result =  GetEntity<Dictionary<int, int>>($"GetCountTasksByUser/{authorId}");
            return result;
        }

        public List<TaskDTO> GetTasksForUser(int userId)
        {
            return GetEntities<TaskDTO>($"GetTasksForUser/{userId}") ;
        }

        public List<TaskDTO> GetFinishedTasksForUser(int userId)
        {
            return GetEntities<TaskDTO>($"GetFinishedTasksForUser/{userId}");

        }

        public List<OlderTeamDTO> GetTeamsWhenMembersOlderThan10Years()
        {
            return GetEntities<OlderTeamDTO>("GetTeamsWhenMembersOlderThan10Years");
        }

        public List<UserTasksDTO> GetUsersAlphabetically()
        {
            return GetEntities<UserTasksDTO>("GetUsersAlphabetically");
        }

        public DataFromUserDTO GetDatafromUser(int userId)
        {
            return GetEntity<DataFromUserDTO>($"GetDatafromUser/{userId}");
        }

        public List<DataFromProjectDTO> GetDataFromProject()
        {
            return GetEntities<DataFromProjectDTO>("GetDataFromProject");
        }
    }
}
