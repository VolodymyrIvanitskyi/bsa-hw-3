﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DAL.Models;
using ProjectStructure.DAL.UnitOfWork.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.DAL.UnitOfWork.Reposity
{
	public class TaskRepository : IRepository<Task>, IDisposable
	{
		private readonly ProjectDBContext _dBContext;

		public TaskRepository(ProjectDBContext dBContext)
		{
			_dBContext = dBContext;
		}
		public void Create(Task task)
		{
			_dBContext.Tasks.Add(task);
		}

		public void Delete(int id)
		{

			var task = _dBContext.Tasks.Find(id);
			_dBContext.Tasks.Remove(task);
		}

		public Task Get(int id)
		{
			return _dBContext.Tasks.Find(id);
		}

		public IEnumerable<Task> GetAll()
		{
			return _dBContext.Tasks.ToList();
		}

		public void Update(Task task)
		{
			_dBContext.Entry(task).State = EntityState.Modified;
		}

		public void Save()
		{
			_dBContext.SaveChanges();
		}

		private bool disposed = false;

		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					_dBContext.Dispose();
				}
			}
			this.disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
